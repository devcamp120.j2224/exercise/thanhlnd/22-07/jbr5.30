package com.example.demo.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.Country;
import com.example.demo.Service.CountryService;

@RestController

public class CountryController {
    @Autowired
    private CountryService countryService;

    @CrossOrigin
    @GetMapping("/country-info")
    public Country getCountryInfo(@RequestParam("countrycode") String countryCode){
        ArrayList<Country> countries = countryService.getCountryList();

        Country findCountry = new Country();
        for(Country country : countries){
            if(country.getCountryCode().equals(countryCode)){
                findCountry = country ;
            }
        }
        return findCountry;
    }
    @CrossOrigin
    @GetMapping("/countries")
    public ArrayList<Country> getCountry(){
        ArrayList<Country> allCountry = countryService.getCountryList();
        return allCountry;
    }
}